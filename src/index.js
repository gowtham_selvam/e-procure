import React from 'react'
import { render } from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { logger } from 'redux-logger'
import allReducers from './reducers/allReducers';
import 'bootstrap/dist/css/bootstrap.css';
import App from "./components/App"

const store = createStore(allReducers, applyMiddleware(thunk, logger))
render(
<Provider store={store}>
   <App />
</Provider>,
document.getElementById('root')
)