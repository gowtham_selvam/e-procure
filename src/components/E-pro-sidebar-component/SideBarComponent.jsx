import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import LandingPage from '../E-pro-landing-page/E-pro-landing-page';
import DashboardComponent from '../E-pro-dashboard-component/DashboardComponent';
import NewPurchaseOrdersComponent from '../E-pro-purchase-orders-component/NewPurchaseOrdersComponent';
import ItemsComponent from '../E-pro-purchase-orders-component/ItemsComponent';
import '../../styles/App.scss';
import { Link } from 'react-router-dom';

// const routes = [
//   {
//     path: '/edge',
//     exact: true,
//     sidebar: () => <div>edge</div>,
//     main: () => <h2>edge</h2>
//   },
//   {
//     path: '/dashboard',
//     sidebar: () => <div>dashboard</div>,
//     main: () => <DashboardComponent/>
//   },
//   {
//     path: '/purchaseorders',
//     sidebar: () => <div>purchaseorders</div>,
//     main: () => <LandingPage/>
//   },
//   {
//     path: '/vendors',
//     sidebar: () => <div>vendors</div>,
//     main: () => <div>vendors</div>
//     // <NewPurchaseOrdersComponent/>
//   },
//   {
//     path: '/payments',
//     sidebar: () => <div>payments</div>,
//     main: () => <h2><ItemsComponent/></h2>
//   },
//   {
//     path: '/support',
//     sidebar: () => <div>support</div>,
//     main: () => <h2>support</h2>
//   },
//   {
//     path: '/configure',
//     sidebar: () => <div>configure</div>,
//     main: () => <h2>configure</h2>
//   }
// ]

class SideBarComponent extends Component {
  constructor() {
    super();
    this.state = {
      currentPage: 0
    }
  }

  
   navigateToPage() {
      this.context.router.push('/purchaseorders')
  };


  render() {
    return (
        <div class = "col-md-1" style = {{ display: "inline-flex" }}>
          <div class="container-fluid" style={{
            padding: '0px',
            // width: '5%',
            // height: '1112px',
            background: '#095AAB'
          }}>
            <ul style={{ listStyleType: 'none', padding: 0, textAlign: "center"}}>
              <li className = "sideBar" onClick={() => this.props.selectItem(item)} style={{ padding: '10px'}}>
                <Link style={{
                  fontSize: 'medium',
                  color: 'white', 
                  textAlign: "center",
                  textDecoration: "none"
                }} to='/edge'>
                  <i className="fab fa-edge"></i>
                </Link>
              </li>
              <li className = "sideBar" style={{ padding: '10px'}}>
                <Link to='/dashboard' style={{
                  fontSize: 'medium',
                  color: 'white', 
                  textAlign: "center",
                  textDecoration: "none"
                }} >
                <i className="far fa-chart-bar"></i>
                <br/>
                Dashboard</Link>
              </li>
              <li className = "sideBar" style={{ padding: '10px'}} onClick = {() => this.navigateToPage } >
                <Link to='/purchaseorders' style={{
                  fontSize: 'medium',
                  color: 'white', 
                  textAlign: "center",
                  textDecoration: "none"
                }}>
                  <i className="fas fa-list-ol"></i>
                <br/>
                  Purchase Orders</Link>
              </li>
              <li className = "sideBar" style={{ padding: '10px'}}>
                <Link to='/vendors' style={{
                  fontSize: 'medium',
                  color: 'white', 
                  textAlign: "center",
                  textDecoration: "none"
                }}>
                <i className="fas fa-archive"></i>
                <br/>
                Vendors</Link>
              </li>
              <li className = "sideBar"
              //  style={{ padding: '10px', height: "1112px"}}
               >
                <Link to='/payments' style={{
                  fontSize: 'medium',
                  color: 'white', 
                  textAlign: "center",
                  textDecoration: "none"
                }}>
                <i className="far fa-credit-card"></i>
                <br/>
                Payments</Link>
              </li>
              <li className = "sideBar" style={{ padding: '10px'}}>
                <Link to='/support' style={{
                  fontSize: 'medium',
                  color: 'white', 
                  textAlign: "center",
                  textDecoration: "none"
                }}>
                <i className="fas fa-life-ring"></i> 
                <br/>
                Support</Link>
              </li>
              <li className = "sideBar" style={{ padding: '10px'}}>
                <Link to='/configure' style={{
                  fontSize: 'medium',
                  color: 'white', 
                  textAlign: "center",
                  textDecoration: "none"
                }} >
                <i className="fas fa-cog"></i>
                <br/>
                Configure</Link>
              </li>
            </ul>

          </div>
          <div 
          // style = {{width: "95%"}}
          >
            {/* {routes.map((route) => (
              <Route
                key={route.path}
                path={route.path}
                exact={route.exact}
                component={route.main}
              />
            ))} */}
          </div>          
        
        </div>
    );

  }
}

export default SideBarComponent;
