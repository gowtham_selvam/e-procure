import {applyMiddleware,createStore} from 'redux';
import React, { Component } from 'react';
import '../../styles/App.scss';
import {connect} from 'react-redux';
import './InfoComponent.scss';
import { fetchDetails } from '../../actions/itemSelect';
import { fetchPOList } from '../../actions/fetchPOList';
import { fetchComments } from '../../actions/fetchCommForSelectedPR';
import { fetchApprovalDetails } from '../../actions/fetchApprovalDetailsForSelectedItem';
import Scrollbars from 'react-custom-scrollbars';

class InfoComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      flagForSelectedItem: false,
      selectedItemRequestId : null
    };
    this.onClickItem = this.onClickItem.bind(this);
    
  }

  componentDidMount() {
    // this.setState = ({
    //   itemList: this.props.itemList
    // })
  }

onClickItem(item){  
  this.setState({selectedItemRequestId: item.requestID});
  this.props.dispatch(fetchDetails(item));
  this.props.dispatch(fetchComments(item));
  this.props.dispatch(fetchApprovalDetails(item));
}

componentWillMount() {
  this.props.dispatch(fetchPOList());
}

componentDidUpdate(prevProps) {  
  if (prevProps.itemList[0] !== this.props.itemList[0]) {
  this.setState({selectedItemRequestId: this.props.itemList[0].requestID});  
  this.props.dispatch(fetchDetails(this.props.itemList[0]));
  this.props.dispatch(fetchComments(this.props.itemList[0]));
  this.props.dispatch(fetchApprovalDetails(this.props.itemList[0]));
  }
}

  render() {
    return (
      <div className="col-md-3 infoComponent">
      {/* <Scrollbars style={{ width: "100%", height: "calc(100% - -28px)" }}> */}
        <div className="infoPanel">
            {this.props.itemList.map((item, id) => {
                  return(
                    <div key={id}  onClick={() => this.onClickItem(item)} id={'selectedCard' + id} className={ 'infoCard '+ 
                      (item.requestID == this.state.selectedItemRequestId ? 'highlighted' : 'notHighlighted')}>
                    <p className="itemInfo">
                      <span className="dot" style={{backgroundColor: item.colorCode}}></span>
                      {item.status}
                    </p>

                    <div className="prInfo">
                      <div className="subInfo1">
                        <label className="prNumber bold">{item.prfRequestID}</label>
                        <label className="creatorInfo">PR Created by {item.requester}</label>
                      </div>
                      <div className="subInfo2">
                        <p className="orderAmount bold">{item.total}</p>
                        <p className = "bold">{item.currency}</p>
                      </div>
                    </div>
                    <p className="orgInfo">{item.vendor}</p>
                  </div>
                  );
              })
            }
        </div>
        {/* </Scrollbars> */}
      </div>
    );

  }
}



const mapStateToProps = (state) => ({ itemList: state.itemList.itemList ,
                                      selectedItem: state.selectedItem.selectedItem })

const mapDispatchToProps = (dispatch) => {
  return {
    selectItem: (item) => {
      dispatch(fetchPosts(item))
    }
  }
}

export default connect(mapStateToProps)(InfoComponent); 