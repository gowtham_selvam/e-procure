import React, { Component } from 'react';
import '../../styles/App.scss';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { selectItem } from '../../actions/itemSelect';
import './InfoComponent.scss';

class InfoComponent extends React.Component {


  render() {
    return (
      <div className="infoComponent">
          <div className="infoPanel">
            {this.props.itemList.map((item, id) => {
              return (
                <div key={id} onClick={() => this.props.selectItem(item)} id={'selectedCard' + id} className={'infoCard ' + (this.props.selectedItem.id == id ? 'highlighted' : 'notHighlighted')}>
                  <p className="itemInfo">
                    <span className="dot"></span>
                    {item.prStatus}
                  </p>

                  <div className="prInfo">
                    <div className="subInfo1">
                      <label className="prNumber bold">PR #{item.prNumber}</label>
                      <label className="creatorInfo">{item.creatorInfo}</label>
                    </div>
                    <div className="subInfo2">
                      <p className="orderAmount bold">{item.orderAmount}</p>
                      <p>INR</p>
                    </div>
                  </div>
                  <p className="orgInfo">{item.organization}</p>
                </div>
              );
            })
            }
          </div>
      </div>
        );
    
      }
    }
    
    
function mapStateToProps(state) {
  return {
          itemList: state.itemList,
        selectedItem: state.selectedItem
      };
    }
    
function matchDispatchToProps(dispatch) {
  return bindActionCreators({selectItem: selectItem }, dispatch)
      }
      
export default connect(mapStateToProps, matchDispatchToProps)(InfoComponent); 