import React from 'react';

export class LogoutComponent extends React.Component {
    render() {
        return (
            <div className="e-pro-logout-notify">
            <i className="fa fa-sign-out fa-2x e-pro-logout-icon" aria-hidden="true"></i>
            <i className="fa fa-bell-o fa-2x e-pro-notify-icon" aria-hidden="true"></i>
            </div>
        )
    }
}