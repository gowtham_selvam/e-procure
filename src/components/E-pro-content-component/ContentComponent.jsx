import React, { Component } from 'react';
// import logo from '../../assets/logo.svg';
import '../../styles/App.scss';

export class ContentComponent extends React.Component {

    render() {
       return (
        <div className="StatusComponent">
                <div>
                        <div className="btn-group" role="group" aria-label="Basic example">
                              <button type="button" className="btn btn-primary StatusButton">Pending</button>
                              <button type="button" className="btn btn-secondary StatusButton">Approved</button>
                        </div>
                        <span className="menuIcon"><i className="fas fa-align-justify"></i></span>
                </div>
        </div>      
       );
    }
  }

//   export default ContentComponent;