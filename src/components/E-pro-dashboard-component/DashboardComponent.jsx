import React from 'react';
import ReactDOM from 'react-dom';
import '../../styles/dashboard.css';
import Highcharts from 'highcharts';
import ReactHighcharts from 'react-highcharts';
import HeaderButton from '../E-pro-header-component/e-pro-header-buttons';
import LogoutComponent from '../E-pro-landing-page/E-pro-landing-page';

const PSbyDepartmentConfig = {
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    yAxis: {
        tickInterval: 20,
        min: 0,
        max: 120
    },
    plotOptions: {
        series: {
            borderRadius: 15,
            color: {
                linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                stops: [
                            [0, '#109BF8'],
                            [1, '#E211F4']
                        ]
                }
        }
    },
    // chart: {
    //     width: 450
    // },
    title : {
        text: 'Payment Status by Department'
    },
    series: [{
        type: 'column',
        data: [101, 82, 95, 89, 70, 79, 68, 101, 82, 95, 90, 110]
    }]
}


const WaitingforApprovalConfig = {
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    title : {
        text: 'Waiting for Approval'
    },
    // chart: {
    //     width: 450
    // },
    plotOptions: {
        series: {
            color: {
                linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                stops: [
                            [0, '#FFB767'],
                            [1, '#FF3A4C']
                        ]
                },
                fillOpacity: 0.3
        }
    },
    series: [{
        type: 'areaspline',
        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 295.6, 454.4]
    }]
}

const TPRconfig = {
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    yAxis: {
        tickInterval: 20,
        min: 0,
        max: 100
    },
    plotOptions: {
        series: {
            color: '#5EE2A0'
        }
    },
    title : {
        text: 'Top Purchase Requests'
    },
    series: [{
        type: 'column',
        data: [101, 82, 95, 89, 70, 79, 68, 101, 82, 95, 90, 110]
    }]
}

const SAconfig = {
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun']
    },
    title : {
        text: 'Spend Analysis'
    },
    plotOptions: {
        area: {
            stacking: 'normal'
        },
        areaspline: {
            fillOpacity: 0.5
        },
        series: {
            color: {
                linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                stops: [
                            [0, '#A3A1FB'],
                            [1, '#54D8FF']
                        ]
                }
        }
    },
    series: [{
        type: 'areaspline',
        name: 'Products sold',
        data: [15, 11, 16, 18, 16, 24],
        fillOpacity: 0.1
    },
    {
        type: 'areaspline',
        name: 'Total views',
        data: [19, 6, 16, 17, 12, 5],
        fillOpacity: 0.1
    }]
}

export default class DashboardComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
    }
    render() {
        return (
            <div class="col-md-11">
            {/* <LogoutComponent/> */}
            <div className="e-pro-new-purchase-order">
                <button className="btn" style={{ color: 'white' }} className="btn btn-outline-primary btn-block"
                onClick={this.newPOCompAction}
                >
                <i className="fa fa-plus"></i> New Purchase Order</button>
                {/* {
                this.props.open === true && 
                //  <NewPOItemsModal/>
                <NewPurchaseOrdersComponent/>
                } */}
            </div>
            <div style = {{ marginTop: "130px" }}> 

                <div className="row">                
                <h5>Requests Status</h5>
                </div>
                    <div className="row">
                                <div className="col-md-2" >
                                <div className="row">
                                    <div className = "box1" style = {{backgroundColor : "#FFB200", width: '198px',
                                height: '160px'}}>
                                        <h6 style = {{color: "white", padding: "10px"}}>Purchase orders issued</h6> <br/>
                                        <h1 style = {{ textAlign: "center", color: "white"}}>34</h1> 
                                    </div>
                                    </div>
                                    <div className="row">
                                    <div className = "box1" style = {{backgroundColor : "#007EFF", width: '198px',
                                height: '160px'}}>
                                        <h6 style = {{color: "white", padding: "10px"}}>Invoices Received</h6> <br/>
                                        <h1 style = {{ textAlign: "center", color: "white" }}>25</h1> 
                                    </div>
                                    </div>
                                </div>
                                    <div className="col-md-6" style = {{ float: 'right'}}>
                                        <ReactHighcharts config={PSbyDepartmentConfig} ref="chart"></ReactHighcharts>
                                    </div>
                                    <div className="col-md-4" >
                                        <ReactHighcharts config={WaitingforApprovalConfig} ref="chart"></ReactHighcharts>
                                    </div>
                    </div>
                    <div className="row"> 
                    <h5>Spend Analysis</h5>
                    </div>
                    <div className = "row">
                                <div className="col-md-5" >
                                    <ReactHighcharts config={TPRconfig} ref="chart"></ReactHighcharts>
                                </div>
                                <div className="col-md-7" style = {{ float: 'right'}}>
                                    <ReactHighcharts config={SAconfig} ref="chart"></ReactHighcharts>
                                </div> 
                    </div>  
                    
            </div>   
                </div>
               );
    }
}
