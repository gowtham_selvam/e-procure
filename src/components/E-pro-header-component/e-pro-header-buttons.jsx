import React from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import {connect} from 'react-redux';
import { showModal, hideModal } from '../../actions/modalActions';
import { newPOCompAction } from '../../actions/newPOItemModalAction';
import '../../styles/App.scss';
import NewPOItemsModal from '../Modals/NewPOItemsModal';
import NewPurchaseOrdersComponent from '../E-pro-purchase-orders-component/NewPurchaseOrdersComponent';
import { push } from 'react-router-redux'

class HeaderButton extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            dropdownOpen: false
        };
    }

    toggle() {
        this.setState(prevState => ({
            dropdownOpen: !prevState.dropdownOpen
        }));
    }

    
    newPOCompAction = () => {
    // this.setState({ open: true });
    // this.props.dispatch(newPOCompAction());
    // location.href="/purchaseorders/newPOrder";
    this.props.dispatch(push('/purchaseorders/newPOrder'));
    console.log("open-->"+this.props.open);
  };

//   onCloseModal = () => {
//     this.props.dispatch(newPOItemModalClose());
//   };

    // openAlertModal(event) {
    //     this.props.dispatch(showModal({
    //      open: true,
    //      title: 'Alert Modal',
    //      message: MESSAGE,
    //      closeModal: this.closeModal
    //    }, 'alert'))
    //   }

    render() {
        return (
            <div>
                <div class ="row">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item e-pro-breadcrumb-inactive"><a href="#">Dashboard</a></li>
                            <li className="breadcrumb-item active" aria-current="page">Orders</li>
                        </ol>
                    </nav>
                </div>
                <div class ="row">
                <div class ="col-md-2">
                <nav className="navbar navbar-light e-pro-searchbar">
                    <form className="form-inline">
                        <input className="form-control mr-sm-2" type="search" placeholder="Search Purchase Orders" aria-label="Search" style={{width:"214px"}} />
                        <i className="fa fa-search" style={{ marginLeft: '-40px' }} aria-hidden="true">
                            <button className="btn btn-outline-success my-2 my-sm-0" style={{ backgroundColor: 'inherit', marginLeft: '-20px', border: 'none' }} type="submit"></button></i>
                    </form>
                </nav>
                </div>
                <div class ="col-md-2">
                <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} className="e-pro-landing-dropdown" style={{
                    // left:"264px",width:"185px",
                    top:"128px", right: "75px"}}>
                    <DropdownToggle caret className="e-pro-landing-dropdown-content">
                        Sort By
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem header>Header</DropdownItem>
                        <DropdownItem disabled>Action</DropdownItem>
                        <DropdownItem>Another Action</DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem>Another Action</DropdownItem>
                    </DropdownMenu>
                </Dropdown>
                </div>
                </div>
                {/* <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} className="e-pro-landing-dropdown" style={{left:"328px",width:"147px"}}> */}
            
    
                <div className="e-pro-new-purchase-order">

                    <button className="btn" style={{ color: 'white' }} className="btn btn-outline-primary btn-block"
 onClick={this.newPOCompAction}
 >
 <i className="fa fa-plus"></i> New Purchase Order</button>
 {/* {
     this.props.open === true && 
    //  <NewPOItemsModal/>
     <NewPurchaseOrdersComponent/>
 } */}
                </div>
                <div className='e-pro-landing-header-grouped-buttons'>
                    <button className="btn" style={{ right: '230px', backgroundColor: '#1EAA68', color: 'white' }}><i className="fa fa-check"></i> Approve</button>
                    <button className="btn" style={{ right: '115px', backgroundColor: '#E57A00', color: 'white' }}><i className="fa fa-pause"></i> Hold</button>
                    <button className="btn" style={{ right: '0px', backgroundColor: '#AC1F25', color: 'white' }}><i className="fa fa-times"></i> Reject </button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({ open: state.newPOReducer.open })

// const mapDispatchToProps = dispatch => ({ 
//     // hideModal: () => dispatch(hideModal()),
//     // showModal: (modalProps, modalType) => {
//     //  dispatch(showModal({ modalProps, modalType }))
//     // }
//     onOpenModal : () => dispatch(onOpenModal()),
//     onCloseModal : () => dispatch(onCloseModal())
//    })

const mapDispatchToProps = (dispatch) => {
    return {
      open: () => {
        dispatch(newPOItemModal())
      }
    }
  }

export default connect(mapStateToProps)(HeaderButton); 

