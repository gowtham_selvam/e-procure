
import React, { Component } from 'react';
import '../../styles/App.scss';

export class HeaderComponent extends React.Component {
    render() {
       return (
          <div className="searchComponent">
               <label>Advanced Search :</label>
               <select className="custom-select" id = "myList" >
               <option value = "" disabled selected>Document number</option>
                 <option value = "1">one</option>
                 <option value = "2">two</option>
                 <option value = "3">three</option>
                 <option value = "4">four</option>
               </select>
  
               <select className="custom-select" id = "myList" >
               <option value = "" disabled selected>Item number</option>
                 <option value = "1">one</option>
                 <option value = "2">two</option>
                 <option value = "3">three</option>
                 <option value = "4">four</option>
               </select>
  
               <select className="custom-select" id = "myList" >
               <option value = "" disabled selected>Due Date</option>
                 <option value = "1">one</option>
                 <option value = "2">two</option>
                 <option value = "3">three</option>
                 <option value = "4">four</option>
               </select>
  
               <select className="custom-select" id = "myList" >
               <option value = "" disabled selected>Amount</option>
                 <option value = "1">one</option>
                 <option value = "2">two</option>
                 <option value = "3">three</option>
                 <option value = "4">four</option>
               </select>
  
               <button type="button" className="btn btn-primary search-btn"> Apply </button>
          </div>
  
       );
    }
  }

  // export default HeaderComponent;
