import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import '../../styles/App.scss';
import classnames from 'classnames';
import EpronavComponent from './e-pro-nav-component';

export default class EpronavContentComponent extends React.Component {
    constructor(props) {
        super(props);

        this.toggleNav = this.toggleNav.bind(this);

        this.state = {
        activeTab: '1'
        };
    }
    toggleNav(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    componentDidMount() {
        this.toggleNav('1');
    }
    render() {
        return (
            <div>            
                <Nav tabs className="e-pro-navtab-header">
                <span className="e-pro-navbar-header-content"><h4>{this.props.navData}</h4></span>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '1' })}
                            onClick={() => { this.toggleNav('1'); }}
                        >
                            <i className="fa fa-info-circle" aria-hidden="true" style={{padding: '5px'}}></i>Summary
                </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '2' })}
                            onClick={() => { this.toggleNav('2'); }}
                        >
                            <i className="fa fa-list" aria-hidden="true" style={{padding: '5px'}}></i>Items
                </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '3' })}
                            onClick={() => { this.toggleNav('3'); }}
                        >
                            <i className="fa fa-file-o" aria-hidden="true" style={{padding: '5px'}}></i>Quotations
                </NavLink>
                </NavItem>
                <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '4' })}
                            onClick={() => { this.toggleNav('4'); }}
                        >
                            <i className="fa fa-thumb-tack" aria-hidden="true" style={{padding: '5px'}}></i>Milestones
                </NavLink>
                </NavItem>
                <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '5' })}
                            onClick={() => { this.toggleNav('5'); }}
                        >
                            <i className="fa fa-paperclip" aria-hidden="true" style={{padding: '5px'}}></i>Documents
                </NavLink>
                    </NavItem>
                <NavItem style={{width: '18% !important'}}>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '6' })}
                            onClick={() => { this.toggleNav('6');}}
                        >
                            <i className="fa fa-id-card" aria-hidden="true" style={{padding: '5px'}}></i> GIIR Approvals
                </NavLink>
                    </NavItem>
                </Nav>   
                <TabContent activeTab={this.state.activeTab} className="e-pro-navbar-body-content">
                    <TabPane tabId="1">
                        <Row>
                            <Col sm="12">
                                <EpronavComponent />
                            </Col>
                        </Row>
                    </TabPane>
                    <TabPane tabId="2">
                        
                    </TabPane>
                    <TabPane tabId="3">
                        
                    </TabPane>
                    <TabPane tabId="4">
                        
                    </TabPane>
                    <TabPane tabId="5">
                        
                    </TabPane>
                    <TabPane tabId="6">
                        
                    </TabPane>
                </TabContent>
            </div>
        );
    }
}

