import React, { Component } from "react";
import "../../styles/App.scss";
export default class NewPOStepsComponent extends Component {
  render() {
    return (
      <div className="col-md-11">
        <button className="btn-nav-active">
          <i className="fas fa-info-circle" /> Summary
        </button>
        <hr class="horizontal" />
        <button className="btn-nav-inactive">
          <i className="fa fa-bars" /> Items
        </button>
        <hr class="horizontal" />
        <button className="btn-nav-inactive">
          <i className="fas fa-file-alt" /> Quotation
        </button>
        <hr class="horizontal" />
        <button className="btn-nav-inactive">
          <i className="fas fa-pen-fancy" /> Milestones
        </button>
        <hr class="horizontal" />
        <button className="btn-nav-inactive">
          <i className="fas fa-paperclip" /> Documents
        </button>
      </div>
    );
  }
}


