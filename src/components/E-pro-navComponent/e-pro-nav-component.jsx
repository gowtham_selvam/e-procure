import React, { Component } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import '../../styles/App.scss';
import { connect } from 'react-redux';
import { PRBasicInfo, tableMetaData, PRBasicInfoTable, PRComments } from '../../constants/ReusableComponents.js';
import moment from 'moment';
import Stepper from 'react-stepper-horizontal';

class EpronavComponent extends React.Component {
    constructor(props) {
        super(props);

        this.toggleNav = this.toggleNav.bind(this);

        // this.state = {
        //     activeTab: '1',
        //     dataList : [],
        //     tableConfig: 
        //     [
        //          {
        //         tableMetaData : tableMetaData,
        //         data: [
        //             {
        //             "requestId" : this.props.selectedItem.requestId,
        //             "vendor" : this.props.selectedItem.vendor,
        //             "shipTo" : this.props.selectedItem.shipTo,
        //             "buyer" : this.props.selectedItem.buyer,
        //             "description" : this.props.selectedItem.description,
        //             "poNumber" : this.props.selectedItem.poNumber,
        //             "status" : this.props.selectedItem.status,
        //             "orgUnit" : this.props.selectedItem.orgUnit,
        //             "requesterSites" : this.props.selectedItem.requesterSites,
        //             "billTo" : this.props.selectedItem.billTo,
        //             "departmentName" : this.props.selectedItem.departmentName
        //             }
        //         ]
        //      }
        //      ] , 
        //     tempArray : []
        // };

        this.state = {  
            activeTab: '1',
        };
            this.state = {
              steps: [{
                title: this.props.aprovalDetails[0].flagIsApproved,
              }, {
                title: 'Approved By DH',
              }, {
                title: 'Awaiting approval from Purchase Committee',
              }, {
                title: 'Finance Head Approval',
              }],
              currentStep: 2,
            };
        // console.log("keys"+this.props.selectedItem);
    }
    toggleNav(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    componentWillMount() {
   
    }

    componentDidMount() {
        this.toggleNav('1');
    }
    render() {
        const { steps, currentStep } = this.state;
        return (
            <div >
                    <Stepper steps = {steps}
                    completeColor= "#186BC8" activeColor="#DAE0E5" activeTitleColor = "#186BC8" completeBarColor="#186BC8"
                    completeTitleColor = "#186BC8" size={32} activeBorderColor = "#186BC8" activeBorderStyle ="solid" defaultTitleColor = "#57A1F2"
                    activeStep={ currentStep } circleFontColor="186BC8" 
                     />
                <div class="e-pro-custom-navtab">
                <div class="container panel panel-default">
                    <div class="mx-2 px-1 py-3 panel-heading">
                    <h5>Basic Information</h5>
                    </div>
                    <div class="container panel-body">
                        <PRBasicInfo label = 'PO Request' result = {this.props.selectedItem.requestId} />
                        <PRBasicInfo label = 'Supplier' result = {this.props.selectedItem.vendor} />
                        <PRBasicInfo label = 'Ship To' result = {this.props.selectedItem.shipTo} />
                        <PRBasicInfo label = 'Buyer' result = {this.props.selectedItem.buyer} />
                        <PRBasicInfo label = 'Description' result = {this.props.selectedItem.description} />
                        <PRBasicInfo label = 'PO Request Code' result = {this.props.selectedItem.poNumber} />
                        <PRBasicInfo label = 'Type' result = {this.props.selectedItem.status} />
                        <PRBasicInfo label = 'Organization Unit' result = {this.props.selectedItem.orgUnit} />
                        <PRBasicInfo label = 'Site' result = {this.props.selectedItem.requesterSites} />
                        <PRBasicInfo label = 'Bill To' result = {this.props.selectedItem.billTo} />
                        <PRBasicInfo label = 'Department' result = {this.props.selectedItem.departmentName} />
                    </div>
                </div>
                </div>
                <div className="e-pro-conversation-panel">
                    <h5 className="e-pro-conversation-header">Comments</h5> 
                    {/* <h6 className="py-1 px-5 e-pro-add-conversation">+ Add Comment</h6>     */}
                    <a className="anchorBreadCrum py-1 px-5 e-pro-add-conversation" style={{color:"blue"}} href="#/profile">+ Add Comment</a>  
                    {
                        this.props.selectedItemComments.map(item => {
                            return <PRComments discussionData = {item.discussionData} 
                            discussionBy = {item.buyerName}
                            discussionTimestamp = {moment(item.discussionDate).format("lll")} />
                        })
                    }                    
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
          selectedItem: state.selectedItem.selectedItem,
          selectedItemComments: state.selectedItemComments.commentList,
          aprovalDetails: state.aprovalDetails.aprovalDetails
    };
}

export default connect(mapStateToProps)(EpronavComponent);