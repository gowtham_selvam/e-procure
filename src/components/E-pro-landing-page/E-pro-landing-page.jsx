import React from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, CardTitle, CardText, Row, Col } from 'reactstrap';
import { Button } from 'reactstrap';
import { Table } from 'reactstrap';

import '../../styles/App.scss';
import '../../styles/index.scss';

import { EpronavComponent } from '../E-pro-navComponent/e-pro-nav-component';
import { FooterComponent } from '../E-pro-footer-buttons/e-pro-footer-buttons';
import { ProgressBarComponent } from '../E-pro-step-progressbar/e-pro-progressbar-component';
import { HeaderComponent } from '../E-pro-header-component/HeaderComponent';
import { ContentComponent } from '../E-pro-content-component/ContentComponent';
import InfoComponent from '../E-pro-sidebar-component/InfoComponent';
import { ProgressBar, Step } from "react-step-progress-bar";
import {EpronavContentComponent} from '../E-pro-navComponent/e-pro-nav-content-component';
import HeaderButton from '../E-pro-header-component/e-pro-header-buttons';
import MainInfoComponent from '../E-pro-main-info-component/MainInfoComponent';
import $ from 'jquery';
import classnames from 'classnames';
import PurchaseOrdersComponent from '../E-pro-purchase-orders-component/PurchaseOrdersComponent';

class LandingPage extends React.Component {
  constructor(props) {
    super(props);

    this.toggleDocument = this.toggleDocument.bind(this);
    this.toggleItem = this.toggleItem.bind(this);
    this.toggleMaterial = this.toggleMaterial.bind(this);
    this.toggleDescription = this.toggleDescription.bind(this);

    this.state = {
      dropdownOpen: false,
      previousSelected: '',
    };


    this.state = {
      activeTab: '1'
    };

    this.state = {
      tableContent: [{
        item: '01',
        rate: '300SGD'
      }, {
        item: '02',
        rate: '500SGD'
      }, {
        item: '03',
        rate: '600SGD'
      }, {
        item: '04',
        rate: '800SGD'
      }, {
        item: '05',
        rate: '200SGD'
      }, {
        item: '06',
        rate: '700SGD'
      }, {
        item: '07',
        rate: '400SGD'
      }, {
        item: '08',
        rate: '300SGD'
      }, {
        item: '09',
        rate: '600SGD'
      }
      ],
      stepDetails: [{
        id: '1',
        Description: 'Step 1 is processed',
        status: 'complete'
      }, {
        id: '2',
        Description: 'Step 2 is processed',
        status: 'complete'
      }, {
        id: '3',
        Description: 'Step 3 is in progress',
        status: 'inprogress'
      }, {
        id: '4',
        Description: 'Step 4 is pending',
        status: 'pending'
      }
      ],
      showProgressBar: false
    }

    this.showProgressComp = this.showProgressComp.bind(this);
  }

  toggleDocument() {
    this.setState(prevState => ({
      documentDropdownOpen: !prevState.documentDropdownOpen
    }));
  }
  toggleItem() {
    this.setState(prevState => ({
      itemDropdownOpen: !prevState.itemDropdownOpen
    }));
  }
  toggleMaterial() {
    this.setState(prevState => ({
      materialDropdownOpen: !prevState.materialDropdownOpen
    }));
  }
  toggleDescription() {
    this.setState(prevState => ({
      descriptionDropdownOpen: !prevState.descriptionDropdownOpen
    }));
  }



  highlightSelected(event) {
    // previousSelected;
    if (this.state.previousSelected != undefined) {
      this.state.previousSelected.classList.value = 'e-pro-custom-table-inActive';
    }
    this.state.previousSelected = event.currentTarget;
    event.preventDefault();
    // $("tr").click(function(){
    //   $(this).addClass("selected").siblings().removeClass("selected");
    // });
    console.log(event)
    event.currentTarget.classList.value = 'e-pro-custom-table-active';
    this.setState({
      rate: event.currentTarget.outerText
    })
  }

  updateDocDropdown(event) {
    this.setState({
      documentContent: event.currentTarget.textContent
    })
  }

  updateItmDropdown(event) {
    this.setState({
      itemContent: event.currentTarget.textContent
    })
  }

  updateMatDropdown(event) {
    this.setState({
      materialContent: event.currentTarget.textContent
    })
  }

  updateDescDropdown(event) {
    this.setState({
      descriptionContent: event.currentTarget.textContent
    })
  }

  componentWillMount() {
    this.state.rate = this.state.tableContent[0].rate;
    this.state.documentContent = 'Document Number',
      this.state.descriptionContent = 'Descriptiom',
      this.state.itemContent = 'Item Number',
      this.state.materialContent = 'Material Number'
  }

  showProgressComp() {
    this.setState({
      showProgressBar: !this.state.showProgressBar
    })
    console.log(this.state.showProgressBar)
  }

  render() {
    return (
      <div class="col-md-11">
        <HeaderButton/>
        <div style = {{ marginTop: "130px" }} >
            <PurchaseOrdersComponent/>
            </div>
      </div>
    );
  }
}

export default LandingPage;
