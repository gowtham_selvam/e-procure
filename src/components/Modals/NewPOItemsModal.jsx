import React from "react";
import { render } from "react-dom";
import Modal from "react-responsive-modal";
import HeaderButton from '../E-pro-header-component/e-pro-header-buttons';
import { connect } from 'react-redux';
import { newPOItemModalClose } from '../../actions/newPOItemModalClose';
import {saveNewItemDataAction} from '../../actions/saveNewItemDataAction';

const styles = {
  fontFamily: "sans-serif",
  textAlign: "center"
};

class NewPOItemsModal extends React.Component {
  state = {
    open: false
  };

//   onOpenModal = () => {
//     this.setState({ open: true });
//   };

//   onCloseModal = () => {
//     this.setState({ open: false });
//   };


onOpenModal = () => {
    // this.setState({ open: true });
    this.props.dispatch(newPOItemModalAction());
    console.log("open-->"+this.props.open);
  };

  onCloseModal = () => {
    // this.props.dispatch(newPOItemModalClose());
    // this.setState({ open : false });
  };

  saveNewItemData = () => {
      this.props.dispatch(saveNewItemDataAction());
  }

  render() {
    // const { open } = this.state;
    return (
      <div style={styles}>
        {/* <button className=" btn btn-add-new-skill" onClick={this.onOpenModal}>
          + Add New Item
        </button> */}
        <Modal open={this.props.open} onClose={this.onCloseModal} center>
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">
              Add New Item
            </h5>
          </div>
          <div class="modal-body">
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="inputEmail4">Item Type </label>
                &nbsp;&#10033;
                <input
                  type="email"
                  class="form-control"
                  id="inputEmail4"
                  placeholder="Item Type"
                />
              </div>
              <div class="form-group col-md-6">
                <label for="inputPassword4">Item Name</label>&nbsp;&#10033;
                <input
                  type="text"
                  class="form-control"
                  id="inputPassword4"
                  placeholder="Enter Name"
                />
              </div>
              <div class="form-group col-md-2">
                <label for="inputPassword4">Revision</label>
                <input
                  type="text"
                  class="form-control"
                  id="inputPassword4"
                />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="inputState">UMO</label>&nbsp;&#10033;
                <select id="inputState" class="form-control">
                  <option selected>Choose...</option>
                  <option>...</option>
                </select>
              </div>
              <div class="form-group col-md-2">
                <label for="inputZip">Quantity</label>&nbsp;&#10033;
                <input type="text" class="form-control" id="inputZip" />
              </div>
              <div class="form-group col-md-6">
                <label for="inputCity">Description as per PO</label>
                &nbsp;&#10033;
                <input type="text" class="form-control" id="inputCity" />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="inputEmail4">Item Price </label>&nbsp;&#10033;
                <input
                  type="email"
                  class="form-control"
                  id="inputEmail4"
                  placeholder="Enter Price"
                />
              </div>
              <div class="form-group col-md-4">
                <label for="inputPassword4">Total Value</label>
                <input
                  type="text"
                  class="form-control"
                  id="inputPassword4"
                />
              </div>
              <div class="form-group col-md-4">
                <label for="inputPassword4">Tax</label>&nbsp;&#10033;
                <input
                  type="text"
                  class="form-control"
                  id="inputPassword4"
                  placeholder="Select Tax"
                />
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button
              type="button"
              class="btn btn-secondary"
              data-dismiss="modal"
              onClick = {this.onCloseModal()}
            >
              Close
            </button>
            <button type="button" class="btn btn-primary" onClick = {this.saveNewItemData()}>
              Save changes
            </button>
          </div>
        </Modal>
      </div>
    );
  }
}

// render(<App />, document.getElementById("root"));


const mapStateToProps = (state) => ({ open: state.newPOItemModal.open  })

// const mapDispatchToProps = dispatch => ({ 
//     // hideModal: () => dispatch(hideModal()),
//     // showModal: (modalProps, modalType) => {
//     //  dispatch(showModal({ modalProps, modalType }))
//     // }
//     onOpenModal : () => dispatch(onOpenModal()),
//     onCloseModal : () => dispatch(onCloseModal())
//    })

const mapDispatchToProps = (dispatch) => {
    return {
      open: () => {
        dispatch(newPOItemModal())
      }
    }
  }

export default connect(mapStateToProps)(NewPOItemsModal); 