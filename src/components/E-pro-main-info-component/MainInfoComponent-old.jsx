import React, { Component } from 'react';
import {connect} from 'react-redux';
import '../../styles/App.scss';

class MainInfoComponent extends React.Component {

      constructor(props) {
            super(props);
            }

            showProgess(event){
                  this.props.showProgressComponent(event);
                }
                
      render() {
            if(!this.props.selectedItem){
                  return (<h4>Select a card...</h4>);
            }
            return (
                  <div className="MainInfoComponent card">
                        <p className="card-header">{this.props.selectedItem.info2}</p>
                        <div className="card-body">
                              <div className="mainInfoCard card-title">
                                    <img className="jtcLogo" src={"https://upload.wikimedia.org/wikipedia/commons/6/66/JTC_Corporation_Logo.svg"} />
                                    <span>
                                          <label className="InfoID">{this.props.selectedItem.info2.split(' ')[2] +"-"+this.props.selectedItem.info2.split(' ')[3]}</label>
                                          <label className="subInfo">Created by Dzung, Verified bu Hao</label>
                                    </span>
                                    <span className="InfoPrice">
                                    <label className="priceAmount">{this.props.selectedItem.info3.split(' ')[0]}</label>
                                    <label className="priceCurrency">{this.props.selectedItem.info3.split(' ')[1]}</label>
                                    <label className={"infoStatus " + (this.props.selectedItem.info6 == "pending" ? 'pendingApproval' : 'Approved')}>{this.props.selectedItem.info6 == "pending" ? 'Pending Approval' : 'Approved'}</label>
                                    </span>
                              </div>
                        </div>
                  </div>      
            );
      }
  }

      function mapStateToProps(state) {
            return {
                  selectedItem: state.selectedItem
            };
      }

      export default connect(mapStateToProps)(MainInfoComponent); 