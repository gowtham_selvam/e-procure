import React, { Component } from 'react';
import {connect} from 'react-redux';
import './MainInfoComponent.scss';
import EpronavContentComponent from '../E-pro-navComponent/e-pro-nav-content-component';
import {Tabs, Tab} from 'react-bootstrap-tabs';
import HorizontalLabelPositionBelowStepper from '../E-pro-step-progressbar/e-pro-progressbar-component';

class MainInfoComponent extends React.Component {

      render() {
            if(!this.props.selectedItem){
                  return (<h4>Select an item...</h4>);
            }
            return (
                  <div className="col-md-9 MainInfoComponent">                  
                        {/* <div>
                        <HorizontalLabelPositionBelowStepper/>
                        </div> */}
                        <div class="row">
                       <div className="col-md-12 mainInfoPanel">
                              <div className="subInfo1">
                                          <p className="prNumber bold">PR-{this.props.selectedItem.requestId}</p>
                                          <p className="creatorInfo">{this.props.selectedItem.requester}</p>
                                          <p className="orgInfo">{this.props.selectedItem.vendor}</p>
                              </div>
                              <div className="subInfo2">
                                          <p className="orderAmount bold">{this.props.selectedItem.total}</p>
                                          <p>{this.props.selectedItem.currencyDetails}</p>
                                          <p className="itemInfo">
                                                <span className="dot" style={{backgroundColor: this.props.selectedItem.colorCode}}></span>
                                                {this.props.selectedItem.status}
                                          </p>
                              </div>
                       </div>
                        </div>
                        <div class="row" style = {{height: "64px"}}>
                       <div className="col-md-12 tabsPanel">
                                    <EpronavContentComponent/>
                        </div>
                        </div>
                  </div>      
            );
      }
  }

      function mapStateToProps(state) {
            return {
                  selectedItem: state.selectedItem.selectedItem
            };
      }

      export default connect(mapStateToProps)(MainInfoComponent); 