import React from 'react';
import ReactDOM from 'react-dom';
import { FontAwesome } from 'react-fontawesome';
import { createStore} from 'redux';
import { Provider } from 'react-redux';

import '../styles/index.scss';

import {EpronavComponent} from './E-pro-navComponent/e-pro-nav-component';
import { FooterComponent } from './E-pro-footer-buttons/e-pro-footer-buttons'
import { ProgressBarComponent } from './E-pro-step-progressbar/e-pro-progressbar-component';
import { HeaderComponent } from './E-pro-header-component/HeaderComponent';
import { ContentComponent } from './E-pro-content-component/ContentComponent';
import { ProgressPage } from './E-pro-progress-page/e-pro-progress-page';
import LandingPage from './E-pro-landing-page/E-pro-landing-page';
import {HeaderButton} from './E-pro-header-component/e-pro-header-buttons';
import {LogoutComponent} from './E-pro-logout-component/e-pro-logout-notification';
import SideBarComponent from './E-pro-sidebar-component/SideBarComponent';
import { BrowserRouter as Router, Route, IndexRoute } from 'react-router-dom';
// import ReactDOM from 'react-dom';
// import LandingPage from '../E-pro-landing-page/E-pro-landing-page';
import DashboardComponent from './E-pro-dashboard-component/DashboardComponent';
import NewPurchaseOrdersComponent from './E-pro-purchase-orders-component/NewPurchaseOrdersComponent';
import ItemsComponent from './E-pro-purchase-orders-component/ItemsComponent';

import 'bootstrap/dist/css/bootstrap.css';

const routes = [
  {
    path: '/edge',
    exact: true,
    sidebar: () => <div>edge</div>,
    main: () => <h2>edge</h2>
  },
  {
    path: '/dashboard',
    sidebar: () => <div>dashboard</div>,
    main: () => <DashboardComponent/>
  },
  {
    path: '/purchaseorders',
    sidebar: () => <div>purchaseorders</div>,
    main: () => <LandingPage/>
  },
  {
    path: '/vendors',
    sidebar: () => <div>vendors</div>,
    main: () => <NewPurchaseOrdersComponent/>
  },
  {
    path: '/payments',
    sidebar: () => <div>payments</div>,
    main: () => <ItemsComponent/>
  },
  {
    path: '/support',
    sidebar: () => <div>support</div>,
    main: () => <h2>support</h2>
  },
  {
    path: '/configure',
    sidebar: () => <div>configure</div>,
    main: () => <h2>configure</h2>
  },
  {
    path: '/purchaseorders/newPOrder',
    sidebar: () => <div>NewPurchaseOrdersComponent</div>,
    main: () => <NewPurchaseOrdersComponent/>
  }
]

class App extends React.Component{

render() {
    return (
      <div className="container-fluid">
         
      <Router>
      <div className="row">
          <SideBarComponent/>
          {/* <Route path="/" component={ App }> */}
           <Route exact path="/" component={DashboardComponent}/>
            {/* <IndexRoute component={ DashboardComponent } /> */}
            { routes.map((route) => (
              <Route
                key={route.path}
                path={route.path}
                exact={route.exact}
                component={route.main}
              />
              ))}
              {/* </Route> */}
             {/* <Route path="/purchaseorders/newPOrder" component={NewPurchaseOrdersComponent} /> */}
        </div>
       </Router> 
      <div class = "column col-md-11">
      <LogoutComponent/>
      {/* <Router>
        <div>
          {/* <SideBarComponent/> */}
            {/* { routes.map((route) => (
              <Route
                key={route.path}
                path={route.path}
                exact={route.exact}
                component={route.main}
              />
              ))}
             {/* <Route path="/purchaseorders/newPOrder" component={NewPurchaseOrdersComponent} /> */}
        {/* </div>
      </Router>  */}
    
     
      </div>
      </div>
    );
  }
}

export default App;