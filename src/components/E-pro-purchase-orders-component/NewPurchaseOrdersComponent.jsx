import React, { Component } from 'react';
import { HeaderComponent } from '../../components/E-pro-header-component/HeaderComponent';
import  InfoComponent  from '../E-pro-sidebar-component/InfoComponent';
import  MainInfoComponent  from '../E-pro-main-info-component/MainInfoComponent';
import HeaderButton from '../E-pro-header-component/e-pro-header-buttons';
import FooterNewPurchaseOrder from '../E-pro-footer-buttons/FooterNewPurchaseOrder';
import '../../styles/App.scss';
import { connect } from 'react-redux';
import HeaderNewPurchaseOrder from '../E-pro-header-component/Header-new-purchase-order';
import NewPOStepsComponent from '../E-pro-navComponent/new-po-steps-component';
import { saveBasicInfoForNewPO } from '../../actions/saveBasicInfoForNewPO';

class NewPurchaseOrdersComponent extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            newPOFormList: []
        };
        this.onChangeOfBuyerName = this.onChangeOfBuyerName.bind(this);
    }

    onChangeOfBuyerName(e) {
          
         this.state.newPOFormList.push(e.target.value);
        
      }

    onClickOfNext() {
        this.props.dispatch(saveBasicInfoForNewPO());
    }

    render() {
            return (
                
<div  class="col-md-11">
<div class="notificationBar">
    </div>  
    <HeaderNewPurchaseOrder/>
  <NewPOStepsComponent/>

<div class= "new-purchase-main-info">
<h1 class="purchaseOrdersHeading"> Buyer Details </h1>
<div class="row">
           <div class="col-md-3">
           <label for="BuyerName" class="purchaseOrderLabels" class=" col-form-label">Buyer Name
            </label>
			</div>
			
			<div class="col-md-3">
           <label for="DepartmentName" class="purchaseOrderLabels">Department<span class="asterisk"> *</span>
            </label>
			</div>
			
			<div class="col-md-3">
           <label for="Organisation" class="purchaseOrderLabels">Organisation Unit<span class="asterisk"> *</span>
            </label>
			</div>
			
			<div class="col-md-3">
           <label for="Owner" class="purchaseOrderLabels">Secondary Owner<span class="asterisk"> *</span>
            </label>
			</div>
			
			</div>
			
			
			<div class="row">
          <div class="col-md-3">
                   <input type="text" class="form-control InputBox" name="firstname" placeholder="Enter Name" 
                //    value={this.props.saveNewPOBasicInfo.prRequester} 
                onChange = {this.onChangeOfBuyerName}
                   required />
          </div>
		  <div class="col-md-3">
                           <select class="form-control InputBox" placeholder="Select Site">
  <option value="Select">Select Department</option>
  <option value="DeptA">Dept A</option>
  <option value="DeptB">Dept B</option>
  <option value="DeptC">Dept C</option>
</select> 
          </div>
		  <div class="col-md-3">
                           <select class="form-control InputBox" placeholder="Select Organisation">
  <option value="Select">Select Organisation</option>
  <option value="OrgA">Org A</option>
  <option value="OrgB">Org B</option>
  <option value="OrgC">Org C</option>
</select> 
          </div>
		  <div class="col-md-3">
                           <select class="form-control InputBox" placeholder="Select Owner">
  <option value="Select">Select Owner</option>
  <option value="Owner1">Kaileys Fund</option>
  <option value="Owner2">Eurassia Plants</option>
  <option value="Owner3">Japan Gardens</option>
</select> 
          </div>
  </div>
  <br/>
  <br/>
  
  <h2 class="purchaseOrdersHeading"> Purchase Details </h2>
  <div class="row">
           <div class="col-md-3">
           <label for="Site" class="purchaseOrderLabels">Site<span class="asterisk"> *</span>
            </label>
			</div>
			
			<div class="col-md-3">
           <label for="Currency" class="purchaseOrderLabels">Currency<span class="asterisk"> *</span>
            </label>
			</div>
			
			<div class="col-md-3">
           <label for="ShipLocation" class="purchaseOrderLabels">Ship-To<span class="asterisk"> *</span>
            </label>
			</div>
			
			<div class="col-md-3">
           <label for="BillLocation" class="purchaseOrderLabels">Bill-To<span class="asterisk"> *</span>
            </label>
			</div>
			
			</div>
			
			
			<div class="row">
          <div class="col-md-3">
		                <select class="form-control InputBox" placeholder="Select Site">
  <option value="Select">Select Site</option>
  <option value="SiteA">Chicago</option>
  <option value="SiteB">Eurassia</option>
  <option value="SiteC">Japan</option>
</select> 
		   </div>
		 
		  <div class="col-md-3">
                    <select class="form-control InputBox" placeholder="Select Currency">
  <option value="Select">Select Currency</option>
  <option value="Currency1">U.S. Dollar</option>
  <option value="Currency2">Euro</option>
  <option value="Currency3">Japanese Yen</option>
</select> 
          </div>
		  <div class="col-md-3">
                     <select class="form-control InputBox" placeholder="Select Location">
  <option value="Select">Select Location</option>
  <option value="ShipLocation1">Korea</option>
  <option value="ShipLocation2">U.S.</option>
  <option value="ShipLocation3">Japan</option>
</select> 
          </div>
  
  <div class="col-md-3">
                    <select class="form-control InputBox" placeholder="Select Location">
  <option value="Select">Select Location</option>
  <option value="BillLocation1">Korea</option>
  <option value="BillLocation2">U.S.</option>
  <option value="BillLocation3">Japan</option>
</select> 
          </div>
		  </div>
  <br/>
  
  
  
  <div class="row">
           <div class="col-md-3">
           <label for="Description" class="purchaseOrderLabels">Description<span class="asterisk"> *</span>
            </label>
			</div>
			
			<div class="col-md-3">
           <label for="tax" class="purchaseOrderLabels">Total tax amount
            </label>
			</div>
			
			<div class="col-md-3">
           <label for="PRF1" class="purchaseOrderLabels">PRF Amount
            </label>
			</div>
			
			<div class="col-md-3">
           <label for="PRF2" class="purchaseOrderLabels">PRF Amount(with taxes)
            </label>
			</div>
			
			</div>
			
			
			<div class="row">
          <div class="col-md-3">
                   <input type="text" class="form-control InputBox" id="Description"  placeholder="Enter Description" 
                   value={this.props.saveNewPOBasicInfo.description} 
                   required />
          </div>
		  <div class="col-md-3">
                   <input type="text" class="form-control InputBox" id="tax"  placeholder="Enter Tax Amount" 
                   value={this.props.saveNewPOBasicInfo.prOtherTaxes} 
                   required />

            <p id="taxP">Enter other taxes(if any)</p>
          </div>
		  <div class="col-md-3">
                   <input type="text" class="form-control InputBox" id="PRF1" placeholder="Enter Amount" 
                   value={this.props.saveNewPOBasicInfo.prOtherTaxes} 
                   required />
          </div>
		  <div class="col-md-3">
                   <input type="text" class="form-control InputBox" id="PRF2" placeholder="Enter Amount"
                    value={this.props.saveNewPOBasicInfo.prOtherTaxes} 
                    required />

          </div>
  </div>
  <br/>
  {/* <p id="requestP">Is this request project related?</p> */}
  {/* <div class ="row">
  <div class="col-md-2">
   <input type="radio" name="response" value="male" checked/>No<br/>
   </div>
   <div class="col-md-2">
   <input type="radio" name="response" value="female"/> Yes<br/>
  </div>  
  </div> */}
  <div class="row">
    <div class="col-md-3">
    <p id="requestP">Is this request project related?</p>
    </div>
    </div>
    <div class="row">
    <div class="col-md-3">
    <label class="btn btn-default">
    <input type="radio" name="response" value="male" checked />
    NO &nbsp;
    </label>
    <label class="btn btn-default">
    <input type="radio" name="response" value="female" /> YES
    </label>
    </div>
</div> 
{/* <FooterNewPurchaseOrder/> */}
<button className="btn e-pro-footer-button-alignment" onClick = {() => this.onClickOfNext()}
        >Next section  <i class="fa fa-angle-right" aria-hidden="true"></i></button>
</div>
</div>

        );
    }
  }
  

  const mapStateToProps = (state) => ({ saveNewPOBasicInfo: state.saveNewPOBasicInfo})

  export default connect(mapStateToProps)(NewPurchaseOrdersComponent);
