import React from 'react';
import { Button } from 'reactstrap';
import '../../styles/App.scss';
import classnames from 'classnames';

export class FooterComponent extends React.Component {
render() {
    return (
        <div className="container e-pro-footer-button">
        <button className="btn e-pro-footer-button-alignment">Reject<img src={require('../../../assets/checkbox_cross.png')}></img></button>
            <button className="btn e-pro-footer-button-alignment">Re-route<img src={require('../../../assets/repeat.png')}></img></button>
            <button className="btn e-pro-footer-button-alignment">Approve <img src={require('../../../assets/checkbox_check.png')}></img></button>
        </div>
    );
}
}