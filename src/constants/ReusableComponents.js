import React from 'react';
import '../styles/App.scss';

export const PRBasicInfo = props => <div class="row">
                                        <div class ="col p-3 mx-auto e-pro-basic-info">{props.label}</div> 
                                        <div class ="col p-3 mx-auto e-pro-nav-values">{props.result}</div>
                                   </div>;

export const PRBasicInfoTable = props => <table>
                                         <tbody>
                                         <tr> 
                                             <th className="e-pro-basic-info">{props.label}</th> 
                                             <td className="e-pro-nav-values">{props.result}</td>
                                         </tr>
                                         </tbody>
                                         </table>;

export const PRComments = props => <div class="panel panel-default pr-5 scrollbar scrollbar-black bordered-black square">
                                   <div class="panel-body e-pro-conversation-block-admin" style = {{textAlign: "left"}}>
                                        {props.discussionData}
                                   </div> 
                                   <h6 class="float-left ml-5 commentTimeStamp">{props.discussionBy}</h6>
                                   <h6 class = "float-right commentTimeStamp">{props.discussionTimestamp}</h6>                   
                                   </div>


export const tableMetaData = [
     {
          "rowName" : "requestId",
          "displayName" : "PO Request"
     },
     {
          "rowName" : "vendor",
          "displayName" : "Supplier"
     },
     {
          "rowName" : "shipTo",
          "displayName" : "Ship To"
     },
     {
          "rowName" : "buyer",
          "displayName" : "Buyer"
     },
     {
          "rowName" : "description",
          "displayName" : "Description"
     },
     {
          "rowName" : "poNumber",
          "displayName" : "PO Request Code"
     },
     {
          "rowName" : "status",
          "displayName" : "Type"
     },
     {
          "rowName" : "orgUnit",
          "displayName" : "Organization Unit"
     },
     {
          "rowName" : "requesterSites",
          "displayName" : "Site"
     },
     {
          "rowName" : "billTo",
          "displayName" : "Bill To"
     },
     {
          "rowName" : "departmentName",
          "displayName" : "Department"
     }

]
