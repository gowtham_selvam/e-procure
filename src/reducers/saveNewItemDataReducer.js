export default function saveNewItemData(state={
    itemList : [{
    requestID:1623 ,
    prfRequestID: "PRF-1487",
    requester: "",
    requesterID: "",
    vendor: "",
    shipTo: "",
    billTo: "",
    currency: "",
    total: 0,
    status: "",
    secondaryOwner: "",
    secondaryOwnerID: "",
    poRevisionNo: 0,
    createdDate: "",
    requesterSite: "",
    isApproved: true,
    statusID: 0,
    departmentID: 0,
    showFlag: "",
    departmentName: "",
    limitationID: 0,
    limitation: "",
    statusShortName: "",
    orgUnit: 0,
    poNumber: "",
    description: "",
    profilePicPath: "",
    isRequester: true,
    isApprover: true,
    orgUnitID: "",
    approvalStatus: "",
    loggedInUser: "07020",
    strBuyer: "",
    countryID: "5",
    prCountryId: ""  
}]  
}, action) {
  switch(action.type){
      case "SAVE_NEW_ITEM_DATA": {
        //   return Object.assign({}, state, {
        //       fetched: true
        //   }
        //      );
        // this.setState({itemList: action.payload});
        return {...state
            , newItemPostData : action.payload 
        };
      }
      case 'FETCH_PO_LIST_FAIL':
          return state;
      default:
          return state
  }
//   return state;
}