import {combineReducers} from 'redux';
import ItemList from './reducer-items';
import SelectedItem from './reducer-selected-item';
import ProcureData from './e-pro-reducer-procure-data';
import newPOItemModal from './newPOItemModal';
import newItemPostData from './saveNewItemDataReducer';
import newPOReducer from './newPOReducer';
import saveNewPOBasicInfo from './saveNewPOBasicInfoReducer';
import selectedItemComments from './selectedItemComments';
import aprovalDetails from './selectedItemApprovalDetails';

const allReducers = combineReducers({
    itemList: ItemList,
    selectedItem: SelectedItem,
    procData: ProcureData,
    newPOItemModal: newPOItemModal,
    newItemPostData: newItemPostData,
    newPOReducer: newPOReducer,
    saveNewPOBasicInfo : saveNewPOBasicInfo,
    selectedItemComments : selectedItemComments,
    aprovalDetails: aprovalDetails
});

export default allReducers;