export default function (state={
    commentList : [
        {
          "requestID": 1615,
          "discussionDate": "2018-11-01T00:00:00",
          "statusId": 2,
          "status": "Alert",
          "discussionData": "API testing",
          "createdBy": "04530",
          "buyerName": "",
          "color": "red",
          "userProfile": "../assets/layouts/layout2/img/UserProfilePics/04530.gif",
          "pulsate": 1,
          "discussionID": 179
        },
        {
          "requestID": 1615,
          "discussionDate": "2018-10-11T00:00:00",
          "statusId": 2,
          "status": "Alert",
          "discussionData": "Same with Surya's comments below .",
          "createdBy": "01584               ",
          "buyerName": "SUMESH BALAKRISHNAN",
          "color": "red",
          "userProfile": "../assets/layouts/layout2/img/UserProfilePics/01584               .gif",
          "pulsate": 1,
          "discussionID": 178
        },
        {
          "requestID": 1615,
          "discussionDate": "2018-10-11T00:00:00",
          "statusId": 2,
          "status": "Alert",
          "discussionData": "Did you clarify this to Kabir in PC ? \" Why do we really need this? If each meeting room has HDMI cable installed, is this investment worth it? What happens to the AirTame stuff?\"",
          "createdBy": "01584               ",
          "buyerName": "SUMESH BALAKRISHNAN",
          "color": "red",
          "userProfile": "../assets/layouts/layout2/img/UserProfilePics/01584               .gif",
          "pulsate": 1,
          "discussionID": 177
        },
        {
          "requestID": 1615,
          "discussionDate": "2018-10-09T00:00:00",
          "statusId": 1,
          "status": "Suggestion",
          "discussionData": "Why do we really need this? If each meeting room has HDMI cable installed, is this investment worth it? What happens to the AirTame stuff? Request you to please provide clarification. Putting this on hold.",
          "createdBy": "04057               ",
          "buyerName": "KABIR CHUGH",
          "color": "lightgreen",
          "userProfile": "../assets/layouts/layout2/img/UserProfilePics/04057               .gif",
          "pulsate": 0,
          "discussionID": 174
        },
        {
          "requestID": 1615,
          "discussionDate": "2018-10-09T00:00:00",
          "statusId": 1,
          "status": "Suggestion",
          "discussionData": "Why do we really need this? If each meeting room has HDMI cable installed, is this investment worth it? What happens to the AirTame stuff? Request you to please provide clarification. Putting this on hold.",
          "createdBy": "04057               ",
          "buyerName": "KABIR CHUGH",
          "color": "lightgreen",
          "userProfile": "../assets/layouts/layout2/img/UserProfilePics/04057               .gif",
          "pulsate": 0,
          "discussionID": 175
        },
        {
          "requestID": 1615,
          "discussionDate": "2018-10-08T00:00:00",
          "statusId": 2,
          "status": "Alert",
          "discussionData": "Checked on the reviews in Amazon and from the image and comments looks like USB is a mode of connection, if NOT a Wi-Fi connectivity.\r\n\r\nWhile the review talks about video resolution..  USB is generally inferior when compared to HDMI.\r\n\r\nBarco doesn't seem to have a HDMI interface...\r\nThe price seems to be exorbitant too...\r\nAre we switching from AirTame to this new device?\r\n\r\nCan't we have a simple laptop screen cast option?\r\n\r\nRegards\r\nSurya\r\n+91 9492179494",
          "createdBy": "07245",
          "buyerName": "SURYAPRASAD KOYYALAMUDI",
          "color": "red",
          "userProfile": "../assets/layouts/layout2/img/UserProfilePics/07245.gif",
          "pulsate": 1,
          "discussionID": 170
        }
      ]
  }, action) {
  switch(action.type){
      case "ITEM_SELECTED_COMMENTS": {
          return {...state, selectedItem : action.payload };
      }
      case 'ITEM_REJECTED_COMMENTS':
          return action.data;
      // default:
      //     return state
  }
  return state;
}
