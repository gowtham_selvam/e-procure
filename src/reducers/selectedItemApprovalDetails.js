export default function (state={
    aprovalDetails : [
        {
        approvedBy: "",
        roleName: "PurchaseCommittee Member",
        approvalStatus: "",
        approvalDate: "",
        flagIsApproved: false
        },
        {
        approvedBy: "SOURAV MOHAPATRA",
        roleName: "FinanceHead",
        approvalStatus: "Approved",
        approvalDate: "06 Nov 2018",
        flagIsApproved: true
        },
        {
        approvedBy: "",
        roleName: "FinanceHead",
        approvalStatus: "Approved",
        approvalDate: "06 Nov 2018",
        flagIsApproved: true
        },
        {
        approvedBy: "SOURAV MOHAPATRA",
        roleName: "Department Head",
        approvalStatus: "Approved",
        approvalDate: "11 Oct 2018",
        flagIsApproved: true
        }
    ]
  }, action) {
  switch(action.type){
      case "ITEM_SELECTED_COMMENTS": {
          return {...state, selectedItem : action.payload };
      }
      case 'ITEM_REJECTED_COMMENTS':
          return action.data;
      // default:
      //     return state
  }
  return state;
}
