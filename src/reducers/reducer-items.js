export default function itemList(state={
    itemList : [{
    requestID: 1623 ,
    prfRequestID: "",
    requester: "",
    requesterID: "",
    vendor: "",
    shipTo: "",
    billTo: "",
    currency: "",
    total: 0,
    status: "",
    secondaryOwner: "",
    secondaryOwnerID: "",
    poRevisionNo: 0,
    createdDate: "",
    requesterSite: "",
    isApproved: false,
    statusID: 0,
    departmentID: 0,
    showFlag: "",
    departmentName: "",
    limitationID: 0,
    limitation: "",
    statusShortName: "",
    orgUnit: 0,
    poNumber: "",
    description: "",
    profilePicPath: "",
    isRequester: false,
    isApprover: false,
    orgUnitID: "",
    approvalStatus: "",
    loggedInUser: "07020",
    strBuyer: "",
    countryID: "5",
    prCountryId: ""  
}]  
}, action) {
  switch(action.type){
      case "FETCH_PO_LIST": {
        //   return Object.assign({}, state, {
        //       fetched: true
        //   }
        //      );
        // this.setState({itemList: action.payload});
        return {...state
            , itemList : action.payload 
        };
      }
      case 'FETCH_PO_LIST_FAIL':
          return state;
      default:
          return state
  }
//   return state;
}
