export default function (state={
      requestId: 1624,
      prRequestCode:"--",
      prDepartmentId:75,
      description:"Supplies for Dallas",
      buyer:"Alice Svetlik",
      vendorId:813,
      shipTo:"Texas",
      billTo:"Texas",
      prShipTo:27,
      prBillTo:27,
      currency:2.0,
      total:491.26,
      statusId:19,
      secondaryOwner:"91021",
      prApprovalDate:"0001-01-01T00:00:00",
      prpoNumber:"",
      poRevNo:1,
      requesterSite:"27",
      orgUnit:12,
      typeId:2,
      limitationId:7,
      projectCode:"",
      vendorName:"Office Depot",
      requester:"91020",
      poNumber:null,
      prfRequestId:"",
      countryName:"US",
      status:"19",
      currencyDetails:"USD",
      supplierAddress:"",
      city:"",
      postalCode:"",
      state:"",
      billToAddress:"Hitachi Consulting Corporation,14643 Dallas Parkway\r\nSuite 800\r\nDallas, TX 75254, United States",
      billToCity:"Dallas",
      billToState:"",
      billToCountry:"US",
      billToPostalCode:"75254",
      shipToAddress:"Hitachi Consulting Corporation,14643 Dallas Parkway\r\nSuite 800\r\nDallas, TX 75254, United States",
      shipToCity:"Dallas",
      shipToState:"",
      shipToCountry:"US",
      shipToPostalCode:"75254",
      createdDate:"2018-10-11T00:00:00",
      userEdit:true
    }, action) {
    switch(action.type){
        case "ITEM_SELECTED": {
            // return Object.assign({}, state, {
            //     fetched: true
            // }
            //    );
            return {...state, selectedItem : action.payload };
        }
        case 'DATA_RECEIVED':
            return action.data;
        // default:
        //     return state
    }
    return state;
}
