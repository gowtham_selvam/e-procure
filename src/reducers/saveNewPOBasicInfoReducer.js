export default function saveNewPOBasicInfo(state={
    requestId: 0,
                prRequestCode: "string",
                prDepartmentId: 0,
                description: "string",
                prRequester: "string",
                prVendorID: 0,
                prShipTo: 0,
                prBillTo: 0,
                prCurrencyId: 0,
                prTotal: 0,
                prStatusID: 0,
                prProjectCode: "string",
                prSecondaryOwner: "string",
                prApprovalDate: "2018-11-28T07:41:54.472Z",
                prpoNumber: "string",
                poRevNo: 0,
                createdDate: "2018-11-28T07:41:54.472Z",
                prCreatedBy: "string",
                modifiedDate: "2018-11-28T07:41:54.472Z",
                prModifiedBy: "string",
                prRequesterSite: "string",
                prIsActive: true,
                prInActiveDate: "2018-11-28T07:41:54.472Z",
                prTypeId: 0,
                prLimitationId: 0,
                prOrgUnit: 0,
                prOnHold: true,
                prCountryId: 0,
                prShippingCharges: 0,
                prOtherTaxes: 0,
                prOtherTaxesName: "string"
}, action) {
  switch(action.type){
      case "SAVE_NEW_PO_BASIC_INFO": {
        //   return Object.assign({}, state, {
        //       fetched: true
        //   }
        //      );
        // this.setState({itemList: action.payload});
        return {...state
            , newItemPostData : action.payload 
        };
      }
      case 'SAVE_NEW_PO_BASIC_INFO_FAIL':
          return state;
      default:
          return state
  }
//   return state;
}