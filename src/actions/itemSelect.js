import axios from "axios";

export function fetchDetails(tempItem) {
    return function (dispatch) {
        axios.get('http://192.168.133.181:8050/api/PurchaseRequests/GetPurchaseRequestDetails?RequestId='+tempItem.requestID+'&CountryId='+tempItem.orgUnit)    
        .then((response) => {
            dispatch({type: "ITEM_SELECTED", payload: response.data[0]})
        })
        .catch((err) => {
            dispatch({type: "ITEM_REJECTED", payload: err})
        })
    }
}
