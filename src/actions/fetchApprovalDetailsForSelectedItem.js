import axios from "axios";

export function fetchApprovalDetails(tempItem) {
    return function (dispatch) {
        axios.get('http://192.168.133.181:8050/api/PurchaseRequests/GetApprovalDetailsForRequestId?RequestId='+tempItem.requestID+'&CountryId='+tempItem.orgUnit)    
        .then((response) => {
            dispatch({type: "GET_APPROVAL_DETAILS_FOR_ITEM", payload: response.data[0]})
        })
        .catch((err) => {
            dispatch({type: "GET_APPROVAL_DETAILS_FOR_ITEM_REJECTED", payload: err})
        })
    }
}
