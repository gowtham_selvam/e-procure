
export function newPOItemModalClose() {
    return function (dispatch) {
        dispatch({type: "CLOSE_NEW_ITEM_MODAL", payload: false})
    }
}