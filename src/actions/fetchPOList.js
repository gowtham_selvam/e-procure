import axios from "axios";

export function fetchPOList() {
    return function (dispatch) {
        // axios.get('http://192.168.133.181:8050/api/PurchaseRequests/GetPurchaseRequestBySearch?RequestId=${item.requestID}&countryId=${item.countryId}') 
        axios.post('http://192.168.133.181:8050/api/PurchaseRequests/GetPurchaseRequestBySearch', {
                requestID: 0,
                prfRequestID: "",
                requester: "",
                requesterID: "",
                vendor: "",
                shipTo: "",
                billTo: "",
                currency: "",
                total: 0,
                status: "",
                secondaryOwner: "",
                secondaryOwnerID: "",
                poRevisionNo: 0,
                createdDate: "",
                requesterSite: "",
                isApproved: true,
                statusID: 0,
                departmentID: 0,
                showFlag: "",
                departmentName: "",
                limitationID: 0,
                limitation: "",
                statusShortName: "",
                orgUnit: 0,
                poNumber: "",
                description: "",
                profilePicPath: "",
                isRequester: true,
                isApprover: true,
                orgUnitID: "",
                approvalStatus: "",
                loggedInUser: "91021",
                strBuyer: "",
                countryID: "12"
              })
        // axios.get('http://192.168.133.181:8050//api/Admin/GetCountryList')
        .then((response) => {
            dispatch({type: "FETCH_PO_LIST", payload: response.data})
        })
        .catch((err) => {
            dispatch({type: "FETCH_PO_LIST_FAIL", payload: err})
        })
    }
}
