import axios from "axios";

export function saveNewItemDataAction() {
    return function (dispatch) {
        // axios.get('http://192.168.133.181:8050/api/PurchaseRequests/GetPurchaseRequestBySearch?RequestId=${item.requestID}&countryId=${item.countryId}') 
        axios.post('http://192.168.133.181:8050/api/Line/SaveLineDetailsBulk', {
            LineDetails: {
                    LineDetailID: "2067",
                    RequestID: "852",
                    Num: "1",
                    Revision: "1",
                    LineType: "1",
                    Item: "API testing New",
                    ItemCategory: [],
                    Description: "Testing",
                    UOM: "1",
                    Quantity: "36",
                    Price: "340000",
                    Value: "1224000",
                    VendorID: "52",
                    CreatedBy: "01313",
                    ModifiedBy: "04530",
                    Total: "1500000",
                    BudgetItem: "0",
                    CountryID: "5"
                 }
              })
        // axios.get('http://192.168.133.181:8050//api/Admin/GetCountryList')
        .then((response) => {
            dispatch({type: "SAVE_NEW_ITEM_DATA", payload: response.data})
        })
        .catch((err) => {
            dispatch({type: "SAVE_NEW_ITEM_DATA_FAIL", payload: err})
        })
    }
}
