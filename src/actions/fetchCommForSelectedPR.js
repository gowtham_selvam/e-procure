import axios from "axios";

export function fetchComments(tempItem) {
    return function (dispatch) {
        axios.get('http://192.168.133.181:8050/api/PurchaseRequests/GetDiscussionDetailsByRequestId?RequestId=1615'
        // +tempItem.requestID
        ) 
        .then((response) => {
            dispatch({type: "ITEM_SELECTED_COMMENTS", payload: response.data[0]})           
        })
        .catch((err) => {
            dispatch({type: "ITEM_REJECTED_COMMENTS", payload: err})
        })
    }
}
