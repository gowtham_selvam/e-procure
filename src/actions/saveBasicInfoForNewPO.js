import axios from "axios";

export function saveBasicInfoForNewPO() {
    return function (dispatch) {
        // axios.get('http://192.168.133.181:8050/api/PurchaseRequests/GetPurchaseRequestBySearch?RequestId=${item.requestID}&countryId=${item.countryId}') 
        axios.post('http://192.168.133.181:8050/api/PurchaseRequests/SavePurchaseRequestDetails ', {
            saveNewPOBasicInfo: [{
                requestId: 0,
                prRequestCode: "",
                prDepartmentId: 0,
                description: "",
                prRequester: "",
                prVendorID: 0,
                prShipTo: 0,
                prBillTo: 0,
                prCurrencyId: 0,
                prTotal: 0,
                prStatusID: 0,
                prProjectCode: "",
                prSecondaryOwner: "",
                prApprovalDate: "",
                prpoNumber: "",
                poRevNo: 0,
                createdDate: "",
                prCreatedBy: "",
                modifiedDate: "",
                prModifiedBy: "",
                prRequesterSite: "",
                prIsActive: false,
                prInActiveDate: "",
                prTypeId: 0,
                prLimitationId: 0,
                prOrgUnit: 0,
                prOnHold: false,
                prCountryId: 0,
                prShippingCharges: 0,
                prOtherTaxes: 0,
                prOtherTaxesName: ""
            }]
              })
        // axios.get('http://192.168.133.181:8050//api/Admin/GetCountryList')
        .then((response) => {
            dispatch({type: "SAVE_NEW_PO_BASIC_INFO", payload: response.data})
        })
        .catch((err) => {
            dispatch({type: "SAVE_NEW_PO_BASIC_INFO_FAIL", payload: err})
        })
    }
}
